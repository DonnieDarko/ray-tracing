// This code is highly based on smallpt
// http://www.kevinbeason.com/smallpt/
// smallpt, a Path Tracer by Kevin Beason, 2008

#include <math.h>		
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <random>
#include <omp.h>
#include <iostream>
#include <random>
#include <iostream>


#define M_PI 3.14159
#define _CRT_SECURE_NO_WARNINGS 1
#define _BOUNCES_CHANCES .9
#define _BOUNCES_NUMBER 10
#define _GLOBAL_ILLUMINATION 5

struct Vec
{
	double x, y, z;		// position, also color (r,g,b)
    Vec (double x_ = 0, double y_ = 0, double z_ = 0)
	{
		x = x_;
		y = y_;
		z = z_;
	}
	Vec operator+ (const Vec & b) const
	{
		return Vec (x + b.x, y + b.y, z + b.z);
	}
	Vec operator- (const Vec & b) const
	{
		return Vec (x - b.x, y - b.y, z - b.z);
	}
	Vec operator* (double b) const
	{
		return Vec (x * b, y * b, z * b);
	}
	Vec operator/ (double b) const
	{
		return Vec (x / b, y / b, z / b);
	}
	Vec mult (const Vec & b) const
	{
		return Vec (x * b.x, y * b.y, z * b.z);
	}
	Vec & norm ()
	{
		return *this = *this * (1 / sqrt (x * x + y * y + z * z));
	}
	double dot (const Vec & b) const
	{
		return x * b.x + y * b.y + z * b.z;
	}				// cross:
	Vec operator% (Vec & b)
	{
		return Vec (y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x);
	}

	double length() const
	{
		return std::sqrt(x * x + y * y + z * z);
	}
};

// Ray Structure
struct Ray
{
	Vec origin, direction;
    Ray (Vec origin_, Vec direction_):origin (origin_), direction (direction_)
	{
	}
};

// Type of materials
enum Refl_t
	{
		DIFF,
		SPEC,
		REFR
	};

// Sphere structure
struct Sphere
{
	double rad;			// radius
	Vec center, emission, color;			// position, emission, color
	Refl_t refl;			// reflection type (DIFFuse, SPECular, REFRactive)

	// Constructor
    Sphere (double rad_, Vec center_, Vec emission_, Vec color_, Refl_t refl_):
		rad (rad_),
		center (center_), emission (emission_), color (color_), refl (refl_)
	{}

	// WARRING: works only if ray.direction is normalized
	// returns distance, 0 if nohit
	double intersect (const Ray & ray) const
	{	// Solve t^2*d.d + 2*t*(origin-center).d + (origin-center).(origin-center)-R^2 = 0		
		Vec op = center - ray.origin;		
		double t;				// distance of intersection along the ray
		double b = op.dot (ray.direction); 
		double det = b * b - op.dot (op) + rad * rad;
		double eps = 1e-4;
		if (det < 0)
			return 0;			// Ray misses the sphere
		else
			det = sqrt (det);	// Return smallest positive t (distance to intersect)
		return (t = b - det) >= eps ? t : ((t = b + det) >= eps ? t : 0); 
	}
};

//Scene: radius, position, emission, color, material
Sphere spheres[] = {		
	Sphere (5., Vec (50, 75, 81.6),				Vec (1e5,1e5,1e5),	Vec (.75, .75, .75),	DIFF),	// light sphere
	Sphere (1e5, Vec (1e5 + 1, 40.8, 81.6),		Vec (),				Vec (.75, .25, .25),	DIFF),	// Left
	Sphere (1e5, Vec (-1e5 + 99, 40.8, 81.6),	Vec (),				Vec (.25, .25, .75)*.9,	DIFF),	// Right
	Sphere (1e5, Vec (50, 40.8, 1e5),			Vec (),				Vec (.25, .75, .25),	DIFF),	// Back
	Sphere (1e5, Vec (50, 40.8, -1e5 + 170),	Vec (),				Vec (),					DIFF),	// Front
	Sphere (1e5, Vec (50, 1e5, 81.6),			Vec (),				Vec (.75, .75, .75),	DIFF),	// Bottom
	Sphere (1e5, Vec (50, -1e5 + 81.6, 81.6),	Vec (),				Vec (.75, .75, .75),	DIFF),	// Top
	Sphere (16.5, Vec (27, 16.5, 47),			Vec (),				Vec (1, 1, 1) * .8,		SPEC),	// Mirror
	Sphere (16.5, Vec (73, 16.5, 78),			Vec (),				Vec (1, 1, 1) * .999,	REFR),	// Glass
};

//Vec light(50, 70, 81.6); // Light point
Vec light_color(1e4, 1e4, 1e4);


inline double
clamp (double x)
{
	return x < 0 ? 0 : x > 1 ? 1 : x;
}

std::default_random_engine generator;
std::uniform_real_distribution<double> distribution(0.0,1.0);

double random_u()
{
	return distribution(generator);
}

Vec sample_cos(double u, double v, Vec n)
{
	// Ugly: create an orthogonal base
	Vec basex, basey, basez;

	basez = n;
	basey = Vec(n.y, n.z, n.x);

	basex = basez % basey;
	basex.norm();

	basey = basez % basex;

	// cosinus sampling. Pdf = cosinus
	return  basex * (std::cos(2. * M_PI * u) * std::sqrt(1 - v)) +
			basey * (std::sin(2. * M_PI * u) * std::sqrt(1 - v)) +
			basez * std::sqrt(v);
}

inline int toInt (double x)
{
	// Convert float to integer to be saved in ppm file
	// 2.2 is a gamma correction
	return int (pow (clamp (x), 1 / 2.2) * 255 + .5);
}

// WARNING: ASSUME NORMALIZED RAY
// Compute the intersection ray / scene.
// Returns true if intersection
// t is defined as the abscisce along the ray 
// (i.e center = ray.origin + t * ray.direction)
// id is the id of the intersected object
inline bool intersect (const Ray & ray, double &t, int &id)
{
	double n = sizeof (spheres) / sizeof (Sphere);
	double direction;
	double inf = t = 1e20;
	for (int i = int (n); i--;)
		if ((direction = spheres[i].intersect (ray)) && direction < t)
		{
			t = direction;
			id = i;
		}
	return t < inf;
}

// return true if there is an object between ray.origin and ray.origin + tmax * ray.direction
inline bool occlude (const Ray & ray, double tmax)
{
	double n = sizeof (spheres) / sizeof (Sphere), direction;
	for (int i = int (n); i--;)
		if ((direction = spheres[i].intersect (ray)) && direction < tmax)
		{
			return true;
		}
	return false;
}

// Reflect the ray i along the normal.
// i should be oriented as "leaving the surface"
Vec reflect(const Vec i, const Vec n)
{
	return n * (n.dot(i)) * 2. - i;
}

double  sin2cos (double x)
{
	return std::sqrt(std::max(0.0, 1.0-x*x));
}

// Fresnel coeficient of transmission.
// Normal point outside the surface
// ior is n0 / n1 where n0 is inside and n1 is outside
double fresnelR(const Vec i, Vec n, double ior)
{
	if(n.dot(i) < 0)
	{
		n = n * -1.;
		ior = 1. / ior;
	}

	double R0 = (ior - 1.) / (ior + 1.);
	R0 *= R0;

	return R0 + (1 - R0) * std::pow(1. - i.dot(n), 5.);
}

// compute refraction vector.
// return true if refraction is possible.
// i and n are normalized
// output wo, the refracted vector (normalized)
// n point outside the surface.
// ior is n0 / n1 where n0 is inside and n1 is outside
bool refract(Vec i, Vec n, double ior, Vec &wo)
{
	i = i * -1.;

	if(n.dot(i) > 0)
	{
		n = n * -1.f;
	}
	else
	{
		ior = 1.f / ior;
	}

    double k = 1.f - ior * ior * (1. - n.dot(i) * n.dot(i));
    if (k < 0.f)
		return false;

	wo = i * ior - n * (ior * n.dot(i) + std::sqrt(k));

	return true;
}

// Hemispheric sampling : Generate random direction on unit hemisphere proportional to solid angle
Vec light_point(Sphere _lightSource, Vec _point){
	Vec localPoint(0,0,0);

	double r1 = random_u();
	double r2 = random_u();

	localPoint.x = cos(2*M_PI*r1) * sqrt(1-r2*r2);
	localPoint.y = sin(2*M_PI*r1) * sqrt(1-r2*r2);
	localPoint.z = r2;

	// Create a coordinate system with z_direction colinear to the direction toward _point
	Vec z_direction = (_point - _lightSource.center).norm();
	Vec basex, basey;

	basey = Vec(z_direction.y, z_direction.z, z_direction.x);
	basex = z_direction % basey;
	basex.norm();
	basey = z_direction % basex;

	// Change coordinate system
	Vec point(0,0,0);
	point = basex*localPoint.x + basey*localPoint.y + z_direction * localPoint.z;
	point.x = _lightSource.center.x + _lightSource.rad * point.x;
	point.y = _lightSource.center.y + _lightSource.rad * point.y;
	point.z = _lightSource.center.z + _lightSource.rad * point.z;

	return point;
}

// Full sphere sampling (not used anymore)
Vec light_point_old(Sphere _lightSource, Vec _point){
	Vec lightPoint(0,0,0);

	double r1 = random_u();
	double r2 = random_u();

	lightPoint.x = _lightSource.center.x + 2 *_lightSource.rad * cosf(2 * M_PI * r1) * sqrtf(r2 * (1-r2));
	lightPoint.y = _lightSource.center.y + 2 *_lightSource.rad * sinf(2 * M_PI * r1) * sqrtf(r2 * (1-r2));
	lightPoint.z = _lightSource.center.z + _lightSource.rad * (1-2*r2);

	return lightPoint;
}

Vec radiance (const Ray &ray, int depth, double _bounced_from_diffuse = 1.)
{
	double t;				// Distance to intersect (calculated 'in place' in 'intersect' function)
	int id;					// Sphere Id that is intersected (calculated 'in place' in 'intersect' function)
	double offset = 0.1;	// Offset in order to suppress surfaces issues due to values imprecision
	Vec color(0,0,0);		// Base radiance is black at first
	
	if (!intersect(ray, t, id) || depth==0){
		// Return a black radiance at the end of iterative bouncing
		// and radiance is calculated only if ray intersect a sphere
		return Vec();
	}
	

	
	// Calculate intersection point between ray and sphere, and normal vector on this point of the surface
	Vec intersect_point = ray.origin + ray.direction*t;
	Vec normal_vector = (intersect_point-spheres[id].center).norm();

	// Sample light using hemispherical surface
	Vec light = light_point(spheres[0], intersect_point);
		
	// Calculate direction and distance to lighting
	Vec directionToLight = (light-intersect_point).norm();
	double distance_to_point = (light-intersect_point).length();
		
	// Calculate new ray and distance to light with a slight offset due to numerical imprecision
	Ray ray_int_point(intersect_point + directionToLight*offset,directionToLight);
	double dist_to_light = (light-(intersect_point+ directionToLight*offset)).length();
		
	// Shadow : shadow_factor changes to 0 in case of occlusion (suppress lighting)
	double shadow_factor = 1.;
	if (occlude(ray_int_point, distance_to_point*0.99)){
		shadow_factor = 0.;
	}

	// Processing diffuse light with color correction
	// multiplying by dot product of normal and ray direction, then dividing by PI  

	// DIFFUSE COLOR
	if (spheres[id].refl == DIFF){

		// Clamp inverse distance to limit high emissive value from light bounces
		double inv_d = 1/std::max(distance_to_point*distance_to_point,1e3);

		// Dot product to check if ray is inside or outside the sphere
		double dot_ray = normal_vector.dot(directionToLight);
		if ((dot_ray*normal_vector.dot(ray.direction)) < 0)
		{
			color = color + spheres[id].color.mult(light_color)*abs(dot_ray)/ M_PI*shadow_factor*inv_d;
			// return null value is shadow_factor == 0 due to occlusion
		}

		// "Russian roulette" : 
		double mean = (spheres[id].color.x + spheres[id].color.y + spheres[id].color.z) / 3.;
		if ( random_u() > mean)
		{
			Vec indirect_light = sample_cos(random_u(),random_u(), normal_vector*-1.0);
			// CAUTION : Simplify costheta/PI term that depends of both bouncing light quantity and sample_cos sampling
			color = color + (spheres[id].color.mult(radiance(Ray(intersect_point+indirect_light*offset, indirect_light),depth-1, 0.)/(1-mean)));
			// Divide radiance by (1-mean) that bouncing equals probabity
			// Set the _bounce_from_diffuse factor to 1. in order not to compute only direct color for specular and refract lighting
		}
	}
		
	// SPECULAR
	if (_bounced_from_diffuse != 1.) // Process only direct lighting
	{
			
		if (spheres[id].refl == SPEC)
		{
			Vec reverb_vec= reflect(ray.direction*-1.f,normal_vector);
			color = spheres[id].emission*(1.-_bounced_from_diffuse) + (spheres[id].color.mult(radiance(Ray(intersect_point+reverb_vec*offset, reverb_vec),depth-1, 0.)));
		}
	}

	// REFRACTION
	if (spheres[id].refl == REFR)
	{
		double	ior = 1.5;
		Vec		base = ray.direction*-1.;
		double	refrac_factor = fresnelR(base,normal_vector, ior);
		Vec		reverb_vec = reflect(base,normal_vector).norm();
		Vec		ref;
		bool	canRefract = refract(base,normal_vector,ior,ref);

		if (random_u() < refrac_factor) // Specular property of glass object
		{
			color = spheres[id].emission*(1.-_bounced_from_diffuse) +(spheres[id].color.mult(radiance(Ray(intersect_point+reverb_vec*offset, reverb_vec), depth-1, 0.)*(refrac_factor/refrac_factor)));
			// Of course refract_factor could be simplified
		}
		else
		{			
			if (canRefract) // Check if refraction can occur depending of ray direction and ior value
			{
				color = spheres[id].emission*(1.-_bounced_from_diffuse) + (spheres[id].color.mult(radiance(Ray(intersect_point+ref*offset, ref), depth-1, 0.)*(1-refrac_factor)/(1-refrac_factor)));
				// Of course (1-refract_factor) could be simplified
			}
		}
	}

	return color;
}

int main (int argc, char *argv[])
{
	// MENU : a few CHOICES to optimize the result
	// Starting with display resolution
	int res_choice;
	int w,h;
	std::cout << "Picture resolution ?\n \t Enter 1 for 800x600\n \t 2 for 1280x900\n \t 3 for 1600x900\n \t 4 for 1920x1080\n \t 5 for 2560x1800\n";
	std::cin >> res_choice;
	switch (res_choice)
	{
		case 1:
			w = 800, h = 600;
			break;
		case 2:
			w = 1280, h = 900;
			break;
		case 3:
			w = 1600, h = 900;
			break;
		case 4:
			w = 1920, h = 1080;
			break;
		case 5:
			w = 2560, h = 1800;
			break;
		default:
			w = 800, h = 600;
	}
	// and samples quantity for better quality or faster processing
	int samples_number;
	std::cout << "How many samples ?\n";
	std::cin >> samples_number;
	int samps = argc == 2 ? atoi (argv[1]) / 4 : samples_number;	// # samples

	// CAMERA SETUP : Position and direction
	Ray cam (Vec (50, 52, 295.6),Vec (0, -0.042612, -1).norm ());
	double fov = 0.5135;						// Field of view
	Vec cx = Vec (w * fov / h);					// x direction increment
	Vec cy = (cx % cam.direction).norm () * fov;	// y direction increment, vector up due to cross product between view direction and horizontal
	Vec *color = new Vec[w * h];					// Vector image resulting from the processing
	
	double perc_y = 0.f;
	
#pragma omp parallel for num_threads(8) // Parallel treatment : Greatly enhance speed processing

	// LOOP OVER ALL IMAGE PIXELS
	for (int y = 0; y < h; y++)	// Loop over columns
    {				
		Vec r;					// Used for color of samples
		fprintf (stderr, "\rRendering (%d spp) %5.2f%%", samps * 4, 100. * perc_y / (h - 1)); // Print progress
#pragma omp atomic
		perc_y++;
		
		for (unsigned short x = 0; x < w; x++)		// Loop over columns
			for (int sy = 0, i = (h - y - 1) * w + x; sy < 2; sy++)	// 2x2 subpixel rows, i is the index of the current pixel
				for (int sx = 0; sx < 2; sx++, r = Vec ())			// 2x2 subpixel cols
				{			
					for (int s = 0; s < samps; s++)	// Loop over samples
					{
						// Apply a filter by determining location of sample within pixel
						double r1 = 2*random_u(), dx = r1 < 1 ? sqrt(r1)-1 : 1-sqrt(2-r1);
						double r2 = 2*random_u(), dy = r2 < 1 ? sqrt(r2)-1 : 1-sqrt(2-r2);
						// Ray direction is determined using cam direction, cx and cy increments and pixel 
						Vec direction =
							cx * (((sx + .5 + dx) / 2 + x) / w - .5) +
							cy * (((sy + .5 + dy) / 2 + y) / h - .5) + cam.direction;
						r =
							r + radiance (Ray (cam.origin + direction * 140, direction.norm ()), _BOUNCES_NUMBER, false) * (1. / samps);
					}		// Camera rays are pushed ^^^^^ forward to start in interior
					color[i] = color[i] + Vec (clamp (r.x), clamp (r.y), clamp (r.z)) * .25;	// Apply gamma correction to the color
				}
    }
	
	// Write out the image to a PPM fie
	FILE *f;
	fopen_s (&f, "image.ppm", "w");
	fprintf (f, "P3\n%d %d\n%d\n", w, h, 255);
	for (int i = 0; i < w * h; i++)
		fprintf (f, "%d %d %d ", toInt (color[i].x), toInt (color[i].y), toInt (color[i].z));
}
